import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class PGNConverter
{

	public static void convert(String originalPath, String convertedPath)
	{
		BufferedReader br = null;
		try
		{
			br = new BufferedReader(new FileReader(originalPath));
		    StringBuilder sb = new StringBuilder();
		    String first;
		    while((first = br.readLine()) != null)
		    {
		    	sb.append(first + "\n");
			    for(int j = 0; j < 10; j++)
			    	sb.append(br.readLine() + "\n");
			    
			    boolean done = false;
			    do
			    {
			    	String line = br.readLine();
			    	if(line == null || line.isEmpty())
			    		done = true;
			    	else
			    		sb.append(line + " ");
			    } while(!done);
			    String s = sb.toString();
			    int index = s.length() - 3;
			    for(; s.charAt(index) != ' '; index--);
			    sb.replace(index - 1, s.length(), "\n\n");
			    PrintWriter writer = new PrintWriter(convertedPath, "UTF-8");
			    writer.println(sb.toString());
			    writer.close();
		    }
		}
		catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			try
			{
				br.close();
			}
			catch (IOException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
		    try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
